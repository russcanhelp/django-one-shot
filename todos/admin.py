from django.contrib import admin
from todos.models import TodoList, TodoItem

admin.site.register(TodoList)
admin.site.register(TodoItem)


class TodoListAdmin:
    list_display = (
        "name",
        "id",
    )


class TodoItemAdmin:
    list_display = (
        "task",
        "due_date",
    )
