from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoForm, TodoItemForm


def the_todolist(request):
    todolists = TodoList.objects.all()
    context = {
        "the_todolists": todolists,
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/todo.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("the_todolist")

    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todo_list(request, id):
    todo_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_instance)
        if form.is_valid:
            form.save()
            return redirect("todo_list_detail", id=todo_instance.id)
    else:
        form = TodoForm(instance=todo_instance)
    context = {"form": form}

    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    todo_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_instance.delete()
        return redirect("the_todolist")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem_instance = form.save()
            return redirect("todo_list_detail", id=todoitem_instance.list_id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    todoitem_instance = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todoitem_instance.list_id)

    else:
        form = TodoItemForm(instance=todoitem_instance)
    context = {"form": form}
    return render(request, "todos/edit_item.html", context)
