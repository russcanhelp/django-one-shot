from django.urls import path
from todos.views import (
    the_todolist,
    todo_list_detail,
    create_todo_list,
    edit_todo_list,
    delete_list,
    todo_item_create,
    todo_item_update,
)

urlpatterns = [
    path("", the_todolist, name="the_todolist"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todo_list, name="create_todo_list"),
    path("<int:id>/edit/", edit_todo_list, name="edit_todo_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
    path("items/edit", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_edit"),
]
